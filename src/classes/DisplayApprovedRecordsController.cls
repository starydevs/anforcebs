public class DisplayApprovedRecordsController {

    public List<ApprovalWrapper> ApprovalProcessList{get;set;}//list to store all approval process

    public String selectedApprovalProcess{get;set;}

    Public list<ProcessInstance> recordList{get;set;} //list to store processInstance records to display on UI

 

    public List<selectoption> getApprovalWrapperList(){

        List<selectoption> temp=new List<selectoption>();
        List<selectoption> temp=new List<selectoption>();
        temp.add(new selectoption('','--Select--'));

        for(String ap:approvalMap.keyset()){

            temp.add(new selectoption(ap,ap));

        }

        return temp;

    }

    public DisplayApprovedRecordsController (){

        ApprovalProcessList=new List<ApprovalWrapper>();

        recordList=new list<ProcessInstance> ();

    }

    public map<String,ApprovalWrapper> approvalMap=new map<String,ApprovalWrapper>();

    public void FetchAllApprovalProcess(){      

        HttpRequest req = new HttpRequest();

        req.setHeader('Authorization', 'Array ' + UserInfo.getSessionID());

        req.setHeader('Content-Type', 'application/json');

        String domainUrl=URL.getSalesforceBaseUrl().toExternalForm();

        system.debug('********domainUrl:'+domainUrl);

        String endpointUrl=domainUrl+'/services/data/v30.0/process/approvals/';

        req.setEndpoint(endpointUrl);

        req.setMethod('GET');      

        Http h = new Http();

        HttpResponse res = h.send(req);

        system.debug(res.getBody());

        String ss=res.getBody();

        string newjsondata = ss.replace('"object"','"objectName"');

       

        // Parse entire JSON response.

        JSONParser parser = JSON.createParser(newjsondata );

        while (parser.nextToken() != null) {

            // Start at the array of invoices.

            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {

                while (parser.nextToken() != null) {

                    // Advance to the start object marker to

                    //  find next approval process object.

                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {

                        // Read entire  approval process object

                        ApprovalWrapper apr= (ApprovalWrapper)parser.readValueAs(ApprovalWrapper.class);

                        system.debug('Approval name: ' + apr.name);

                        system.debug('Id: ' + apr.id);

                        ApprovalProcessList.add(apr);

                        // Skip the child start array and start object markers.

                        parser.skipChildren();

                    }

                }

            }

        }

        system.debug('********ApprovalProcessList:'+ApprovalProcessList);

        for(ApprovalWrapper aw:ApprovalProcessList){

            approvalMap.put(aw.name,aw);

        }

 }

 public Pagereference FindRecords(){

  recordList=new list<ProcessInstance> ();

        if(selectedApprovalProcess!=null && selectedApprovalProcess!=''){

            recordList=new list<ProcessInstance> ();

            String queryString='Select Id, TargetObjectId,TargetObject.name, Status From ProcessInstance Limit 10000';

            System.debug('*********queryString:'+queryString);

            List<ProcessInstance> temp=Database.query(queryString);

            ApprovalWrapper aw=new ApprovalWrapper();

            aw=approvalMap.get(selectedApprovalProcess);

            String ObjName=aw.objectName;

            recordList=new List<Processinstance>();

            For(ProcessInstance sb:temp){

                String ObjectName=String.valueof(sb.TargetObjectId.getSObjectType());

                system.debug('****ObjectName:'+ObjectName);

                if(Objectname.equalsignorecase(ObjName)){

                    recordList.add(sb);

                }

            }

            System.debug('*********recordList:'+recordList);

       }

        return null;

    }    

    public class ApprovalWrapper{

        public String description{get;set;}

        public String id{get;set;}

        public String name{get;set;}

        public String objectName{get;set;}

        public Integer sortOrder{get;set;}

    }

}