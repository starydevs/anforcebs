public class Tracker_Util {
    
    /**********************************************************************************************
         Purpose: For Populating Tracker_Share record. By default tracker record can be accessible 
                  by queue owner but as per the Business Requirement, it should be visible to the 
                  user who has submitted the record so he/she can see it's status from tracker UI.
    ***********************************************************************************************/
    
   /* public static void trackerShare(List<Tracker__c > newTrackerList)
    {
        List<Tracker__Share> lstTrackerShare=new List<Tracker__Share>();
        for(Tracker__c t:newTrackerList)
        {
        System.debug('submitted by' + t.Submitted_By__c);
        Tracker__Share ts=new Tracker__Share();
        ts.ParentId = t.Id;
        ts.AccessLevel = 'Read';
        ts.RowCause = Schema.Tracker__Share.RowCause.Tracker_share__c; //custom Apex sharing reason
        ts.UserOrGroupId = t.Submitted_By__c;
        lstTrackerShare.add(ts); 
        }

    Try{
        if(lstTrackerShare.size()>0)
        {
            insert lstTrackerShare;
        }
    }
    catch(exception e)
    {
        System.debug('Error Message' + e.getMessage());
    }
  }
    
    /*************************************************************************************************
       Purpose: To Insert and Delete Approver records in Approver(child) object
    **************************************************************************************************/ 	
    /*public static void syncApproversForQueue(List<Tracker__c> lstTR, Set<Id> QueueOwnerId)
	{
		
         //List<Group> grp = [Select Id, Name, Email from Group where
                               Type = 'Queue' AND Id In: QueueOwnerId];*///Populating all required Queue ID
            /*List<Id> grpId = new List<Id>(); 
            List<Approver__c> lstapprover=new List<Approver__c>();//List of Approver's to insert
            set<id> trackerIds= new set<id>();
            List<Approver__c> deleteList=new List<Approver__c>();// List of approver's to delete
            Map<Id,List<id>> queueMember=new Map<Id,List<id>>();// Map of Queue ID and List of Queue Memebers
          /*  for(Group gr:grp) 
            {
                grpId.add(gr.Id); 
            }*/
           /* List<GroupMember> gm=[SELECT GroupId,Id,SystemModstamp,UserOrGroupId 
                                  FROM GroupMember WHERE GroupId In:grpId]; *///Populating queueMember
           /*List<GroupMember> gm=[SELECT GroupId,Id,SystemModstamp,UserOrGroupId 
                                  FROM GroupMember WHERE GroupId In:QueueOwnerId]; 
            for(GroupMember gMem:gm) // This block generate Map of Queue ID and List of Queue Memebers
            {
                if(queueMember.containsKey(gMem.GroupId))
                {
                    List<Id> gidAdd=queueMember.get(gMem.GroupId);
                    gidAdd.add(gMem.UserOrGroupId);
                    queueMember.put(gMem.GroupId,gidAdd); //QueueId and its Users
                }
                else
                {
                    List<Id> gid=new List<Id>();
                    gid.add(gMem.UserOrGroupId);
                    queueMember.put(gMem.GroupId,gid);
                }
            }
            System.debug('queueMember'+queueMember);
            
            //Adding IDs of tracker object in set
            
			for(Tracker__c tc:lstTR ){
				trackerIds.add(tc.id);
			}
			//Generating delete List
			deleteList=[select id,Tracker__c from Approver__c where Tracker__c
						In:trackerIds];
			System.debug('deleteList'+deleteList);
			If(deleteList!=NULL && deleteList.size() >0){ 
				Delete deleteList;
			}
			
			//Below block creates list of approvers to insert.  
			for(Tracker__c tc:lstTR ){

				List<Id> lstQueueMemberId=queueMember.get(tc.OwnerId);
				System.debug('lstQueueMemberId'+lstQueueMemberId);
				If(lstQueueMemberId!=NULL){
					for(Id queueMemberId:lstQueueMemberId){
						Approver__c ap=new Approver__c();
						ap.User__c=queueMemberId;
						ap.Tracker__c=tc.id;
						lstapprover.add(ap);
					}
					System.debug('lstapprover'+lstapprover);
				}
				else{
						Approver__c ap1=new Approver__c();
						ap1.User__c=tc.OwnerId;
						ap1.Tracker__c=tc.id;
						lstapprover.add(ap1);
				}
					
			}
			if(lstapprover.size() > 0 && lstapprover!=NULL){
				insert lstapprover;
			}
}
    /*************************************************************************************************
       Purpose: To Insert and Delete Approver records in Approver(child) object
    **************************************************************************************************/
	/*public static void syncApproversForApprovalProcess(){
		
	}
	
	/*************************************************************************************************
       Purpose: To Insert and Delete Approver records in Approver(child) object 
    **************************************************************************************************/
	/*public static void syncApproversForStatus(){
		
		
	}*/

}