/**************************************************************************************************************************************
    Class Name: FAApprovalTriggerHandler
    
    Purpose:    1. This class is Handler class for FAA Approval Trigger 
                
                Please maintain only the last 5 history changes/modifications in the audit log
    
    History of Changes:                 
    -----------------------------------------------------------------------------------------------------------------------------------
        Date                                Developer                               Comments
    -----------------------------------------------------------------------------------------------------------------------------------
                                Sankeerth                               Added Initial code.
   
**************************************************************************************************************************************/
public with sharing class FAApprovalTriggerHandler 
{
    
   /*  public static void insertTrackerData(list<Foreign_Account_Approval__c> lstFAANew, map<id,Foreign_Account_Approval__c> faaOldMap)
     { 
         List<Foreign_Account_Approval__c> LstFAA = new List<Foreign_Account_Approval__c>();
         Set<id> idFAA = new Set<id>();
         Map<Id,Tracker__c> trackerMap = new Map<Id,Tracker__c>();
         Map<Id,String> approvalData = new Map<Id,String>();
         
         for (Foreign_Account_Approval__c faa : lstFAANew) 
         {
                // Access the "old" record by its ID in Trigger.oldMap
                Foreign_Account_Approval__c oldfaa = faaOldMap.get(faa.Id);

                String oldFAAStatus = oldFaa.Status__c;
                String newFAAStatus = faa.Status__c;
                
                // Check that the Status field was changed 
                if (oldFAAStatus != newFAAStatus) 
                {
                  idFAA.add(faa.id);
                  LstFAA.add(faa);
                }
          }
         
         if(idFAA.size() > 0 )
         {
             trackerMap=Tracker_Util.existingTrackerData(idFAA);
             //approvalData=Tracker_Util.getCurrentApproval(idFAA,Constants.FAAObjName);
         }
       
         list<wrapperUtility> wrapList = new list<wrapperUtility>();
         if(LstFAA.size() > 0 )
         {

             for(Foreign_Account_Approval__c faaObj : LstFAA) 
             {
                 //String appOwnerId;
                 //String appOwnerName;
                 
                wrapperUtility wrapObj = new wrapperUtility();*/
                  
                /*if(approvalData.containsKey(faaObj.Id))
                { 
                    appOwnerId = approvalData.get(faaObj.Id).substringBefore(Constants.strTilt);
        			appOwnerName = approvalData.get(faaObj.Id).substringAfter(Constants.strTilt);
                }*/
                 //system.debug('appOwnerId '+appOwnerId + 'appOwnerName '+appOwnerName);
                /*if(trackerMap.containsKey(faaObj.Id))
                { 
                    wrapObj.trackObj = trackerMap.get(faaObj.Id); 
                    wrapObj.isNew = false;
                }
                else
                {
                    wrapObj.trackObj = new Tracker__c();
                    wrapObj.isNew = true;
                }
                 wrapObj.ownerIds = faaObj.OwnerId;
                 wrapObj.TrackerName = faaObj.Name;
                 wrapObj.Status = faaObj.Status__c;
                 set<String> openStatus = Tracker_Util.getStatusValue(Constants.FAAOpenStatus); //Create Faa_open in custom setting, add in constants

                 if(openStatus.contains(faaObj.Status__c))
                 {
                     wrapObj.approvalStatus = Constants.openStatus;
                 }
                 else
                 {
                     wrapObj.approvalStatus = Constants.closeStatus;
                 }
                 
                 wrapObj.ObjectName = Constants.FAAObjName; ----//Add FAAObjName as 'Foreign Account Approval' in constants
                 wrapObj.SubmittedBy = faaObj.CreatedById;
                 wrapObj.recordID = faaObj.Id;
                 wrapObj.lastActivityPerformedBy = faaObj.LastModifiedById;
				 wrapObj.lastActivity = faaObj.LastModifiedDate;
                 wrapObj.Description = faaObj.Description__c; 
                 //wrapObj.approvalOwnerID = appOwnerId;
                 //wrapObj.approvalOwner = appOwnerName;
                	wrapList.add(wrapObj);
                 /*Faa_open
                 wrapObj.Dateassigned = true;
                 */
               
/*             }
         }
         
         if(wrapList.size() > 0 )
         {
             List<Tracker__c> trackLst = Tracker_Util.buildTrackerRecordBulk(wrapList);
             if(trackLst .size() > 0 )
             {
                 upsert trackLst;
                 system.debug('trackLst '+ trackLst);
             }
         }*/
        /*Set<id> QueueOwnerIds = new Set<id>();
	    List<Tracker__c> changedOwnerTracker = new List<Tracker__c>();

        if(trackLst.size() > 0)
        { 
             for(Tracker__c wrapObj :trackLst)
            {
                QueueOwnerIds.add(wrapObj.OwnerIds);
            }
            for(Tracker__c wrapObj:trackLst)
            {
                //changedOwnerTracker.add(wrapObj);
  
                if(isNew)
                {// If new Tracker record is created then create child record without checking tracker old owner
                     changedOwnerTracker.add(wrapObj);
                }
                else 
                { // Update the child record if Tracker owner gets changed
                    if(wrapObj.ownerIds != oldOwner.get(wrapObj.recordID))
                    {
                        changedOwnerTracker.add(wrapObj);
                    } 
                }
            }
            Tracker_Util.syncApproversForQueue(changedOwnerTracker,QueueOwnerIds);
        }
     }*/

}