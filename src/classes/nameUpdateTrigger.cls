public class nameUpdateTrigger {

    /*trigger nameUpdate on Alt_Inv_Suitability_Request__c (after insert,After Update) {
    if( Trigger.IsInsert)
    {
        for (Alt_Inv_Suitability_Request__c ais: Trigger.new)
        {
            Alt_Inv_Suitability_Request__c a = new Alt_Inv_Suitability_Request__c(Id = ais.Id);
            a.Name = ais.RequestID__c;
            update a;
        }      
    } 
    
      /*****************************************Trigger Addition*****************************************
		Developer:   Sankeerth Dharmakkola
		Description: Create and update tracker record on the basis of 
					 status change in AIS approval record.
					 This trigger fires upon after insert and after update events.
      *****************************************************************************************************/
    
   /* Map<Id,Tracker__c> TrackerMap = new Map<Id,Tracker__c>();//Map of AIS record ids and tracker record will be used in after update
    Map<Id,Id> oldOwner=new Map<Id,Id>();
    Boolean isNew=false;
    List<String> idAIS=new List<String>(); //List of id of AIS record. It will use while updating tracker record     
    
    List<Tracker__c> lstTR=new List<Tracker__c>();// List of tracker record to insert or update
    if(Trigger.IsUpdate)// Logic to execute after update
    {				
        
        for(Alt_Inv_Suitability_Request__c aid:Trigger.New)
        {
            idAIS.add(aid.id);
        }
        for(Id a:Trigger.oldMap.keyset())
        {
            Alt_Inv_Suitability_Request__c tmpOldRec=Trigger.oldMap.get(a);
            oldOwner.put(a,tmpOldRec.OwnerId);
        }
        
        for(Tracker__c tPopulate:[Select id,Record_ID__c,OwnerId,Status__c,
                                  Last_Activity_Performed_By__c,Last_Activity_Date__c 
                                  from Tracker__c Where Record_ID__c In: idAIS])
        {// populate already created tracker record and perform operation on existing/populated record
            TrackerMap.put(tPopulate.Record_ID__c,tPopulate);
        }
        
            List<Alt_Inv_Suitability_Request__c> lstAIS = [Select Id,OwnerId,Name,Request_Status__c,Submitted_By__c,
                                                           LastModifiedById,LastModifiedDate,Product__c,ais_client_name__c,
                                                           Product__r.name,ais_client_name__r.name 
                                                           From Alt_Inv_Suitability_Request__c Where Id in : trigger.new];

        for(Alt_Inv_Suitability_Request__c ar: lstAIS)
        {
            
            Tracker__c t=new Tracker__c();
            if(TrackerMap.containsKey(ar.Id))
            { // To  Check whether record is already exist on Tracker record.
                t = TrackerMap.get(ar.Id); 
                isNew=false;
            }
            else
            {//IF record doesn't exist then create new record for tracker record.
                t=new Tracker__c();
                isNew=true;
            }
            /**Update or insert tracker record **/
            /*t.OwnerId = ar.OwnerId;
            t.Name = ar.Name;
            t.Status__c = ar.Request_Status__c;
            if(ar.Request_Status__c == 'Approved' || ar.Request_Status__c == 'Rejected' || 
               ar.Request_Status__c == 'Incomplete' || ar.Request_Status__c == 'Submitted' || ar.Request_Status__c == 'On Hold')
            {
                t.Approval_Status__c = 'Open';
            }
            else
            {
                t.Approval_Status__c = 'Closed';
            }

            t.Object_Name__c = 'AIS Request';
            t.Submitted_By__c = ar.Submitted_By__c;
            t.Record_ID__c = ar.Id;
            t.Last_Activity_Performed_By__c = ar.LastModifiedById;
            t.Last_Activity_Date__c = ar.LastModifiedDate;
            t.Description__c = ar.Product__r.name  +'-'+ ar.ais_client_name__r.name;
            lstTR.add(t);
        }
        
	    if(lstTR.size() > 0)
	    {
	       // Upsert Tracker record
			upsert lstTR;                             
	    }
	    
	    Set<id> QueueOwnerId=new Set<id>();
	    List<Tracker__c> changedOwnerTracker=new List<Tracker__c>();

        if(lstTR.size() > 0)
        { 
             for(Tracker__c t :lstTR)
            {
                QueueOwnerId.add(t.OwnerId);
            }
            for(Tracker__c t:lstTR)
            {
                //changedOwnerTracker.add(t);
  
                if(isNew)
                {// If new Tracker record is created then create child record without checking tracker old owner
                     changedOwnerTracker.add(t);
                }
                else 
                { // Update the child record if Tracker owner gets changed
                    if(t.OwnerId != oldOwner.get(t.Record_ID__c))
                    {
                        changedOwnerTracker.add(t);
                    } 
                }
            }
            Tracker_Util.syncApproversForQueue(changedOwnerTracker,QueueOwnerId);
        }
    }
}*/
}