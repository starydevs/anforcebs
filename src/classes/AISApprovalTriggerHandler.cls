/**************************************************************************************************************************************
    Class Name: AISApprovalTriggerHandler
    
    Purpose:    1. This class is Handler class for AIS Approval Trigger 
                
                Please maintain only the last 5 history changes/modifications in the audit log
    
    History of Changes:                 
    -----------------------------------------------------------------------------------------------------------------------------------
        Date                                Developer                               Comments
    -----------------------------------------------------------------------------------------------------------------------------------
                                Sankeerth                               Added Initial code.
   
**************************************************************************************************************************************/
public with sharing class AISApprovalTriggerHandler 
{
   //This is another testing 
     /*public static void insertTrackerData(list<Alt_Inv_Suitability_Request__c> lstAISNew, map<id,Alt_Inv_Suitability_Request__c> aisOldMap)
     { 
         List<Alt_Inv_Suitability_Request__c> LstAIS = new List<Alt_Inv_Suitability_Request__c>();
         Set<id> idAIS = new Set<id>();
         Map<Id,Tracker__c> trackerMap = new Map<Id,Tracker__c>();
        // Map<Id,String> approvalData = new Map<Id,String>();
         
         for (Alt_Inv_Suitability_Request__c ais : lstAISNew) 
         {
                // Access the "old" record by its ID in Trigger.oldMap
                Alt_Inv_Suitability_Request__c oldais = aisOldMap.get(ais.Id);

                String oldAISStatus = oldAis.Request_Status__c;
                String newAISStatus = ais.Request_Status__c;
                
                // Check that the Status field was changed 
                if (oldAISStatus != newAISStatus) 
                {
                  idAIS.add(ais.id);
                  LstAIS.add(ais);
                }
          }
         
         if(idAIS.size() > 0 )
         {
             trackerMap=Tracker_Util.existingTrackerData(idAIS);
             //approvalData=Tracker_Util.getCurrentApproval(idAIS,Constants.AISObjName);
         }
       
         list<wrapperUtility> wrapList = new list<wrapperUtility>();
         if(LstAIS.size() > 0 )
         {

             for(Alt_Inv_Suitability_Request__c aisObj : LstAIS) 
             {
                 //String appOwnerId;
                 //String appOwnerName;
                 
                wrapperUtility wrapObj = new wrapperUtility();
                  
                /*if(approvalData.containsKey(aisObj.Id))
                { 
                    appOwnerId = approvalData.get(aisObj.Id).substringBefore(Constants.strTilt);
        			appOwnerName = approvalData.get(aisObj.Id).substringAfter(Constants.strTilt);
                }*/
                 //system.debug('appOwnerId '+appOwnerId + 'appOwnerName '+appOwnerName);
              /*  if(trackerMap.containsKey(aisObj.Id))
                { 
                    wrapObj.trackObj = trackerMap.get(aisObj.Id); 
                    wrapObj.isNew = false;
                }
                else
                {
                    wrapObj.trackObj = new Tracker__c();
                    wrapObj.isNew = true;
                }
                 wrapObj.ownerIds = aisObj.OwnerId;
                 wrapObj.TrackerName = aisObj.Name;
                 wrapObj.Status = aisObj.Request_Status__c;
                 set<String> openStatus = Tracker_Util.getStatusValue(Constants.AISOpenStatus); //Create Ais_open in custom setting, add in constants

                 if(openStatus.contains(aisObj.Request_Status__c))
                 {
                     wrapObj.approvalStatus = Constants.openStatus;
                 }
                 else
                 {
                     wrapObj.approvalStatus = Constants.closeStatus;
                 }
                 
                 wrapObj.ObjectName = Constants.AISObjName; //Add AISObjName as 'AIS Request' in constants
                 wrapObj.SubmittedBy = aisObj.CreatedById;
                 wrapObj.recordID = aisObj.Id;
                 wrapObj.lastActivityPerformedBy = aisObj.LastModifiedById;
				 wrapObj.lastActivity = aisObj.LastModifiedDate;
                 wrapObj.Description = aisObj.Description__c;
                 //wrapObj.approvalOwnerID = appOwnerId;
                 //wrapObj.approvalOwner = appOwnerName;
                	wrapList.add(wrapObj);
                 /*Ais_open
                 wrapObj.Dateassigned = true;
                 */
               
            // }
       //  }
         
         /*if(wrapList.size() > 0 )
         {
             List<Tracker__c> trackLst = Tracker_Util.buildTrackerRecordBulk(wrapList);
             if(trackLst .size() > 0 )
             {
                 upsert trackLst;
                 system.debug('trackLst '+ trackLst);
             }
         }
        Set<id> QueueOwnerIds = new Set<id>();
	    List<Tracker__c> changedOwnerTracker = new List<Tracker__c>();

        if(trackLst.size() > 0)
        { 
             for(Tracker__c wrapObj :trackLst)
            {
                QueueOwnerIds.add(wrapObj.OwnerIds);
            }
            for(Tracker__c wrapObj:trackLst)
            {
                //changedOwnerTracker.add(wrapObj);
  
                if(isNew)
                {// If new Tracker record is created then create child record without checking tracker old owner
                     changedOwnerTracker.add(wrapObj);
                }
                else 
                { // Update the child record if Tracker owner gets changed
                    if(wrapObj.ownerIds != oldOwner.get(wrapObj.recordID))
                    {
                        changedOwnerTracker.add(wrapObj);
                    } 
                }
            }
            Tracker_Util.syncApproversForQueue(changedOwnerTracker,QueueOwnerIds);
        }
     }*/
//this is test
}