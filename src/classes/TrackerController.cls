public with sharing class TrackerController{
    //This is the fresh testing 1
    //This is the fresh testing 2
    /*****************************************Tracker controler*****************************************
        Developer:   Sankeerth Dharmakkola
        Purpose:     This controller class is to retrieve tracker records depending upon 
                     object and filter selection
     *****************************************************************************************************/   
    //Defining getter and setter for VF variables
   /* public string selectedTrackerRecord{get;set;}
    public string selectedFilterBy{get;set;}
    public integer SelectedRowNumber{get;set;}
    public List<TrackerWrapper> twp{get;set;}
    public List<TrackerWrapper> twpOpen{get;set;}
    map<string,string> userMap=new map<string,string>(); //map of user id's/queue id's and user/queue name
    public Integer counter{get;set;}
    public boolean isHide{get;set;}
    public boolean isUnHide{get;set;}
    public boolean isNoHideUnHide{get;set;} 
    public boolean isClosedToday{get;set;}
    
    //TrackerController constructor
    public trackerController() 
    {
        //setting render variables, Here Table2 (isNoHideUnHide=true;)is diplayed for default approval records
        isNoHideUnHide=true;
        isHide=true;
        isUnHide=false;
        isClosedToday=false;
        selectedTrackerRecord='All';
        selectedFilterBy='Open Items';   
        //calling this method to display defualt approval records (pending approvals)
    
        twp=new List<TrackerWrapper>(); //list of Wrapper which contain values to display on VF page
        twpOpen=new List<TrackerWrapper>();
        //creating user map to get the name of User to whom approval has been assigned.
        for(user u:[select id,name from user])
        {
            userMap.put(u.id,u.name);
        }
        //creating queue map to get the name of queue to whom approval has been assigned
        for(Group g :[select id,name from Group where type='Queue'])
        {
            userMap.put(g.id,g.name);
        }
        System.debug(userMap);
        showTable();
    }
    //This method is to get Tracker picklist
    public List<SelectOption> getTrackerRecords()
    {
        List<SelectOption> trackerList=new List<SelectOption>();
        trackerList.add(new SelectOption('All','All'));
        trackerList.add(new SelectOption('AIS Request','AIS Request'));
        return trackerList;
    }
    //This getter method is to get Filter by picklist
    public List<SelectOption> getFilterByRecords()
    {
        List<SelectOption> selectStatus=new List<SelectOption>();
        selectStatus.add(new SelectOption('Open Items','Open Items'));
        selectStatus.add(new SelectOption('Closed Today','Closed Today'));
        selectStatus.add(new SelectOption('Unhide','Unhide'));
        return selectStatus;
    }  
    //This method returns the wrapper list to diplay table on selection of object name and filter by value
    public void showTable()
    {   
        //Setting All render variables to false ,table will not be displayed
        isNoHideUnHide=false;	
        isHide=false;
        isUnHide=false;
        isClosedToday=false;
        twp=new List<TrackerWrapper>();//Resetting tracker list
        twpOpen=new List<TrackerWrapper>();

        //List of approvers those who are logged in and is_visible attribue is false (they havn't hidden their records)
        List<Approver__c> lstApprove=[Select tracker__c,User__C,Is_Visible__c from Approver__c 
                                      where User__C =: UserInfo.getUserId() AND Is_Visible__c=false];	
        
        //List of Logged in approvers for hidden records (Is_Visible__c=True)
        List<Approver__c> lstUnhideApprove=[Select tracker__c,User__C,Is_Visible__c from Approver__c 
                                            where User__C =: UserInfo.getUserId() AND Is_Visible__c=True];        
        List<id> trackerId=new List<Id>(); //list of tracker id's
        
        //Iterating over the Approver list to get the tracker records which are not hidden for approvers
        for(Approver__c app:lstApprove)
        {
            trackerId.add(app.tracker__c);
        }
        //Iterating over the approver list to get tracker records which are hidden for approvers
        List<id> trackerUnhideid=new List<Id>();
        for(Approver__c app:lstUnhideApprove)
        {
            trackerUnhideid.add(app.tracker__c);
        }
        counter=0; //setting counter, will be used to count Wrapper list records 
        List<Tracker__c> lstTracker=new List<Tracker__c>(); //list of tracker records
        List<Tracker__c> lstPendingTracker=new List<Tracker__c>(); 
        //checking what user/approver has selected in Object name and filter by value ,depening on selection generating Tracker List
        //This If block check if user has selected Tracker Name(not ALL) and Filter by value is Open,Pending Tracker,Approved Today Or Unhide
        //Depending upon selection it generate the tracker list
        if(selectedTrackerRecord!='All')
        {
            if(selectedFilterBy == 'Open Items')
            {
                lstTracker=[Select Name,id,Record_ID__c,OwnerId,Date_Assigned__c,Approval_Status__c,
                            Status__c,Object_Name__c,description__c From Tracker__c
                            Where id In:trackerId AND  Object_Name__c =: selectedTrackerRecord 
                            AND Approval_Status__c=:'Open'];
                
                lstPendingTracker=[Select Name,id,Record_ID__c,OwnerId,Date_Assigned__c,Approval_Status__c,
                                   Status__c,Object_Name__c,description__c From Tracker__c
                                   Where Object_Name__c =: selectedTrackerRecord AND Approval_Status__c=:'Open'];
                
                isHide=true;
                isNoHideUnHide=true;
            }
            else if(selectedFilterBy == 'Closed Today')
            {
                lstTracker=[Select Name,id,Record_ID__c,OwnerId,Date_Assigned__c,Approval_Status__c,
                            Status__c,Object_Name__c,description__c From Tracker__c
                            Where Object_Name__c =: selectedTrackerRecord AND Approval_Status__c='Closed' 
                            AND (Status__c='Fulfilled' or Status__c='completed')
                            AND LastModifiedDate=Today];   
                isClosedToday=true;		
            }
            else if(selectedFilterBy == 'Unhide')
            {
                lstTracker=[Select Name,id,Record_ID__c,OwnerId,Date_Assigned__c,Approval_Status__c,
                            Status__c,Object_Name__c,description__c From Tracker__c
                            Where id In:trackerUnhideid AND  Object_Name__c =: selectedTrackerRecord];         
                
                isUnHide=true;								
            }
        }
        //This Else block check if user has selected ALL ( not Tracker Name) and Filter by value is Open,Pending Tracker,Approved Today Or Unhide
        //Depending upon selection it generate the tracker list
        else
        {
            if(selectedFilterBy == 'Open Items')
            {
                lstTracker=[Select Name,id,Record_ID__c,OwnerId,Date_Assigned__c,
                            Status__c,Object_Name__c,description__c From Tracker__c
                            Where id In:trackerId AND Approval_Status__c=:'Open'];
                
                lstPendingTracker=[Select Name,id,Record_ID__c,OwnerId,Date_Assigned__c,Approval_Status__c,
                                   Status__c,Object_Name__c,description__c From Tracker__c
                                   Where Approval_Status__c=:'Open'];
                isHide=true;                
                isNoHideUnHide=true;
            }
            else if(selectedFilterBy == 'Closed Today')
            {
                lstTracker=[Select Name,id,Record_ID__c,OwnerId,Date_Assigned__c,Approval_Status__c,
                            Status__c,Object_Name__c,description__c From Tracker__c
                            Where Approval_Status__c='Closed' 
                            AND (Status__c='Fulfilled' or Status__c='completed') 
                            AND LastModifiedDate=Today];
                isClosedToday=true;
            }
            else if(selectedFilterBy == 'Unhide')
            {
                lstTracker=[Select Name,id,Record_ID__c,OwnerId,Date_Assigned__c,Approval_Status__c,
                            Status__c,Object_Name__c,description__c From Tracker__c
                            Where id In:trackerUnhideid];
                isUnHide=true;
            }        
        }
        // Iterate over the tracker list created and map the fields to Wrapper variables which will be sent to VF to display
        for(Tracker__c tc:lstTracker)
        {
            System.debug('lstTracker'+lstTracker);
            String tDate=string.valueOfGmt(tc.Date_Assigned__c);
                    System.debug(userMap);
            counter++;
            TrackerWrapper tw=new TrackerWrapper(tc.Name,tc.id,tc.Record_ID__c,tc.Object_Name__c,tDate,tc.Status__c,userMap.get(tc.OwnerId),tc.Description__c);//generating wrapper list
            tw.counterWrap=counter; //Adding counter to Wrapper list to count number of records
            twp.add(tw);
        }
           System.debug('lstPendingTracker'+lstPendingTracker);
                    System.debug(userMap);
            for(Tracker__c tc:lstPendingTracker)
            {
                String tDate=string.valueOfGmt(tc.Date_Assigned__c);
                if(!userMap.containsKey(tc.OwnerId))
				{
					System.debug(tc.id);
				}
                
                TrackerWrapper tw=new TrackerWrapper(tc.Name,tc.id,tc.Record_ID__c,tc.Object_Name__c,tDate,tc.Status__c,userMap.get(tc.OwnerId),tc.Description__c);//generating wrapper list       
                twpOpen.add(tw);
            }           
        
        
    }
    //This method is to Hide and unhide tracker record
    public PageReference hidingRow(){
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        TrackerWrapper tw;
        //Iterating over the Wrapper list to get the row / tracker record selected by user and remove it from the list
        for(Integer i=0;i<twp.size();i++){
            if(twp[i].counterWrap == param ){
                tw=twp[i];
                twp.remove(i);  
            }
        }
        //Approver record who want to hide or unhide the record
        Approver__C approverHide=[Select Name,id,Tracker__c,User__c,Is_Visible__c from Approver__C where Tracker__c=:tw.trackerId AND User__c=:userInfo.getUserID()];   
        Boolean isFlag;
        //setting flag for Hide and Unhide(Is_Visible__c attribute )
        if(approverHide.Is_Visible__c==false)
        {
            isFlag=true;
        }
        else if(approverHide.Is_Visible__c==true)
        {
            isFlag=false;
        }
        //Updating Is_Visible__c attribue for the approver to hide and uhide the record
        approverHide.Is_Visible__c=isFlag;         
        update approverHide;   
        System.debug('tw'+tw);
        counter--;
        return null;    
    }*/
    
}