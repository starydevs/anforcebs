/**************************************************************************************************************************************
    Class Name: CCApprovalTriggerHandler
    
    Purpose:    1. This class is Handler class for CC Approval Trigger 
                
                Please maintain only the last 5 history changes/modifications in the audit log
    
    History of Changes:                 
    -----------------------------------------------------------------------------------------------------------------------------------
        Date                                Developer                               Comments
    -----------------------------------------------------------------------------------------------------------------------------------
                                Sankeerth                               Added Initial code.
   
**************************************************************************************************************************************/
public with sharing class CCApprovalTriggerHandler 
{
    
    /* public static void insertTrackerData(list<Client_Complaint__c> lstCCNew, map<id,Client_Complaint__c> ccOldMap)
     { 
         List<Client_Complaint__c> LstCC = new List<Client_Complaint__c>();
         Set<id> idCC = new Set<id>();
         Map<Id,Tracker__c> trackerMap = new Map<Id,Tracker__c>();
         //Map<Id,String> approvalData = new Map<Id,String>();
         
         for (Client_Complaint__c cc : lstCCNew) 
         {
                // Access the "old" record by its ID in Trigger.oldMap
                Client_Complaint__c oldcc = ccOldMap.get(cc.Id);

                String oldCCStatus = oldCc.Status__c;
                String newCCStatus = cc.Status__c;
                
                // Check that the Status field was changed 
                if (oldCCStatus != newCCStatus) 
                {
                  idCC.add(cc.id);
                  LstCC.add(cc);
                }
          }
         
         if(idCC.size() > 0 )
         {
             trackerMap=Tracker_Util.existingTrackerData(idCC);
             //approvalData=Tracker_Util.getCurrentApproval(idCC,Constants.CCObjName);
         }
       
         list<wrapperUtility> wrapList = new list<wrapperUtility>();
         if(LstCC.size() > 0 )
         {

             for(Client_Complaint__c ccObj : LstCC) 
             {
                 //String appOwnerId;
                 //String appOwnerName;
                 
                wrapperUtility wrapObj = new wrapperUtility();
                  
                /*if(approvalData.containsKey(ccObj.Id))
                { 
                    appOwnerId = approvalData.get(ccObj.Id).substringBefore(Constants.strTilt);
        			appOwnerName = approvalData.get(ccObj.Id).substringAfter(Constants.strTilt);
                }*/
                 //system.debug('appOwnerId '+appOwnerId + 'appOwnerName '+appOwnerName);
               /* if(trackerMap.containsKey(ccObj.Id))
                { 
                    wrapObj.trackObj = trackerMap.get(ccObj.Id); 
                    wrapObj.isNew = false;
                }
                else
                {
                    wrapObj.trackObj = new Tracker__c();
                    wrapObj.isNew = true;
                }
                 wrapObj.ownerIds = ccObj.OwnerId;
                 wrapObj.TrackerName = ccObj.Name;
                 wrapObj.Status = ccObj.Status__c;
                 set<String> openStatus = Tracker_Util.getStatusValue(Constants.CCOpenStatus); //Create Cc_open in custom setting, add in constants

                 if(openStatus.contains(ccObj.Status__c))
                 {
                     wrapObj.approvalStatus = Constants.openStatus;
                 }
                 else
                 {
                     wrapObj.approvalStatus = Constants.closeStatus;
                 }
                 
                 wrapObj.ObjectName = Constants.CCObjName; //Add CCObjName as 'Client Complaint' in constants
                 wrapObj.SubmittedBy = ccObj.CreatedById;
                 wrapObj.recordID = ccObj.Id;
                 wrapObj.lastActivityPerformedBy = ccObj.LastModifiedById;
				 wrapObj.lastActivity = ccObj.LastModifiedDate;
                 wrapObj.Description = ccObj.Description__c;
                 //wrapObj.approvalOwnerID = appOwnerId;
                 //wrapObj.approvalOwner = appOwnerName;
                	wrapList.add(wrapObj);
                 /*Cc_open
                 wrapObj.Dateassigned = true;
                 */
               
             //}
        // }
         
        /* if(wrapList.size() > 0 )
         {
             List<Tracker__c> trackLst = Tracker_Util.buildTrackerRecordBulk(wrapList);
             if(trackLst .size() > 0 )
             {
                 upsert trackLst;
                 system.debug('trackLst '+ trackLst);
             }
         }
        Set<id> QueueOwnerIds = new Set<id>();
	    List<Tracker__c> changedOwnerTracker = new List<Tracker__c>();

        if(trackLst.size() > 0)
        { 
             for(Tracker__c wrapObj :trackLst)
            {
                QueueOwnerIds.add(wrapObj.OwnerIds);
            }
            for(Tracker__c wrapObj:trackLst)
            {
                //changedOwnerTracker.add(wrapObj);
  
                if(isNew)
                {// If new Tracker record is created then create child record without checking tracker old owner
                     changedOwnerTracker.add(wrapObj);
                }
                else 
                { // Update the child record if Tracker owner gets changed
                    if(wrapObj.ownerIds != oldOwner.get(wrapObj.recordID))
                    {
                        changedOwnerTracker.add(wrapObj);
                    } 
                }
            }
            Tracker_Util.syncApproversForQueue(changedOwnerTracker,QueueOwnerIds);
        }
     }*/

}