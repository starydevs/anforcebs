public class ClientComplaintApproverTrigger {
/*trigger ClientComplaintApprover on Client_Complaint__c (after update) {


    List<String> idCClst = new List<String>(); //List of id of Client Complaint records. It will use while updating tracker record     

    List<Tracker__c> lstTR = new List<Tracker__c>();// List of tracker record to insert or update

    Boolean isNew = false;

    Map<Id,Id> oldOwner = new Map<Id,Id>();

    Map<Id,Tracker__c> TrackerMap = new Map<Id,Tracker__c>();//Map of Client Compliant record ids and tracker record will be used in after update

    

    if(Trigger.isUpdate)

    {

        for(Client_Complaint__c ccid:Trigger.New)

        {

            idCClst.add(ccid.id);

        }

        for(Id a:Trigger.oldMap.keyset())

        {

            Client_Complaint__c tmpOldRec = Trigger.oldMap.get(a);

            oldOwner.put(a,tmpOldRec.OwnerId);

        }

        
        for(Tracker__c tPopulate:[Select id,Record_ID__c,OwnerId,Status__c,

                                  Last_Activity_Performed_By__c,Last_Activity_Date__c 

                                  from Tracker__c Where Record_ID__c In: idCClst])

        {// populate already created tracker record and perform operation on existing/populated record

            TrackerMap.put(tPopulate.Record_ID__c,tPopulate);

        }

        
        for(Client_Complaint__c cc: trigger.new)

        {
            
            Tracker__c t = new Tracker__c();

            if(TrackerMap.containsKey(cc.Id))

            { // To  Check whether record is already exist on Tracker record.

                t = TrackerMap.get(cc.Id); 

                isNew = false;

            }

            else

            {//IF record doesn't exist then create new record for tracker record.

                t = new Tracker__c();

                isNew = true;

            }

            /**Update or insert tracker record **/

           /* t.OwnerId        =  cc.OwnerId;

            t.Name           =  cc.Name;

            t.Status__c      =  cc.Status__c;

            if(cc.Status__c == 'New' || cc.Status__c == 'Pending Branch Review' || 

                                        cc.Status__c == 'Pending Compliance Review')

            {

                t.Approval_Status__c = 'Open';

            }

            else

            {

                t.Approval_Status__c = 'Closed';

            }
        
            t.Object_Name__c                =  'Client Complaint';

            t.Submitted_By__c               =  cc.CreatedById;

            t.Record_ID__c                  =  cc.Id;

            t.Last_Activity_Performed_By__c =  cc.LastModifiedById;

            t.Last_Activity_Date__c         =  cc.LastModifiedDate;

            lstTR.add(t);

        }
        
        if(lstTR.size() > 0)

        {
            // Upsert Tracker record

            upsert lstTR;                             

        }        

        Set<id> QueueOwnerId = new Set<id>();

        List<Tracker__c> changedOwnerTracker = new List<Tracker__c>();

        if(lstTR.size() > 0)

        { 

            for(Tracker__c t :lstTR)

            {

                QueueOwnerId.add(t.OwnerId);

            }

            for(Tracker__c t:lstTR)

            {

                //changedOwnerTracker.add(t);

                if(isNew)

                {// If new Tracker record is created then create child record without checking tracker old owner

                    changedOwnerTracker.add(t);

                }

                else 

                { // Update the child record if Tracker owner gets changed

                    if(t.OwnerId != oldOwner.get(t.Record_ID__c))

                    {

                        changedOwnerTracker.add(t);

                    } 

                }

            }

            Tracker_Util.syncApproversForQueue(changedOwnerTracker,QueueOwnerId);

        }

    }

    
}*/


}